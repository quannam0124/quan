import '/styles/globals.scss';
import React from 'react';
import {Provider} from 'react-redux'

function MyApp({ Component, pageProps }) {
  return (
  <Component {...pageProps} />
  );
}

export default MyApp;
