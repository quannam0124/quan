import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { LOGIN } from '../../../constants/auth';
import styles from '../../../styles/login.module.css';
import { useForm } from 'react-hook-form';
const Landing1st = () => {
  const [hide, setHide] = useState(true);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = (data) => {
    console.log(data);
    dispatch({
      type: LOGIN,
      payload: data,
    });
  };

  const dispatch = useDispatch();
  return (
    <div className={styles['landing-1st']}>
      <div className={styles['Reactangle1']}></div>
      <div className={styles['Reactangle2']}></div>
      <div className={styles['landing-1st-BG-wavy']}>
        <img src="Wavy.png" alt='' />
      </div>
      <div className={styles['stars']}>
        <img src="Stars.png" alt='' />
      </div>
      <img className={styles['vang']} src="vang.png" alt=''></img>
      <div className={styles['cautim']}>
        <img className={styles['tim']} src="tim.png" alt=''></img>
        <img className={styles['xanh']} src="xanh.png" alt=''></img>
      </div>
      <div className={styles['container']} style={{ width: 1440 }}>
        <div className={styles['register']}>
          <div className={styles['create-name']}>Đăng nhập</div>
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className={styles['info']}>
              <div className={styles['email']}>
                <h6 className={styles['emailtxt']}>Email</h6>

                <input
                  className={styles['input-email']}
                  placeholder='Nhập email của bạn'
                  type='email'
                  {...register('email', { required: true })}
                ></input>
                {errors.email && (
                  <span className={styles['erros']}>
                    * This field is required
                  </span>
                )}
              </div>
              <div className={styles['pass']}>
                <h6 className={styles['passtxt']}> Mật khẩu</h6>
                <input
                  type={hide ? 'password' : 'text'}
                  className={styles['input-pass']}
                  placeholder='Nhập nhập mật khẩu mới của bạn'
                  type='password'
                  {...register(
                    'password',

                    { required: true, minLength: 6 },
                  )}
                ></input>

                <img
                  className={styles['hidden-pass']}
                  src="pass.png"
                  alt=''
                  onClick={() => {
                    setHide((hide) => !hide);
                  }}
                ></img>
                {errors.password && (
                  <span className={styles['erros']}>
                    * Password trên 6 ký tự
                  </span>
                )}
              </div>
            </div>
            <div className={styles['check']}>
              <input
                type='checkbox'
                className={styles['checkb']}
              ></input>
              <div className={styles['name-check']}>
                Lưu thông tin đăng nhập
              </div>
            </div>
            <button className={styles['btlogin']} type='submit'>
              <div className={styles['regis']}>Đăng nhập</div>
            </button>
          </form>

          <img className={styles['or']} src="or.png" />
          <img className={styles['social']} src="social.png"></img>
          <div className={styles['login']}>
            Bạn chưa có tài khoản
            <Link className={styles['link']} to='/register'>
              {' '}
              Đăng ký
            </Link>{' '}
            ngay
          </div>
        </div>
      </div>
    </div>
  );
};

export default Landing1st;
