import { useSelector } from 'react-redux';
import Contact from '../../components/Contact';
import LoadingGlobal from '../../components/LoadingGlobal';
import FormLogin from './Components/Login.js';
import styles from '../../styles/login.module.css';

ReactDOM.render(
  <StrictMode>
    <Provder store = {store}>
      <App />
    </Provder>
  </StrictMode>,
  document.getElementById('root')
)

function Login() {
  const loadingAuth = useSelector((state) => state.auth.loadingAuth);
  if (loadingAuth) {
    return <LoadingGlobal />;
  }
  return (
    <div className={styles['App']}>
      <div className={styles['BG-NavBar']}>
        <div className={styles['NavBar'] + ' ' + styles['container']}>
          <div className={styles['logo']}>
            <span>edunet</span>
          </div>
        </div>
      </div>
      <div className={styles['container']}>
        <FormLogin />
        <Contact />
      </div>
    </div>
  );
}

export default Login;
