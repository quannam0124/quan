import React, { useState } from 'react';
import styles from './../styles.module.scss';
import tim from '../../../assets/imgs/tim.png';
import xanh from '../../../assets/imgs/xanh.png';
import vang from '../../../assets/imgs/vang.png';
import pass from '../../../assets/imgs/pass.png';
import or from '../../../assets/imgs/hoac.png';
import social from '../../../assets/imgs/social.png';
import { useDispatch } from 'react-redux';
import { REGISTER } from '../../../constants/auth';
import wavy from '../../../assets/imgs/Wavy.png';
import star from '../../../assets/imgs/Stars.png';
import { useForm } from 'react-hook-form';

const Landing1st = () => {
  const [hide, setHide] = useState(true);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const onSubmit = (data) => {
    console.log(data);
    dispatch({
      type: REGISTER,
      payload: data,
    });
  };
  const dispatch = useDispatch();
  return (
    <div className={styles['landing-1st']}>
      <div className={styles['Reactangle1']}></div>
      <div className={styles['Reactangle2']}></div>
      <div className={styles['landing-1st-BG-wavy']}>
        <img src={wavy} alt='' />
      </div>
      <div className={styles['stars']}>
        <img src={star} alt='' />
      </div>
      <img className={styles['vang']} src={vang} alt=''></img>
      <div className={styles['cautim']}>
        <img className={styles['tim']} src={tim} alt=''></img>
        <img className={styles['xanh']} src={xanh} alt=''></img>
      </div>
      <div className={styles['container']} style={{ width: 1440 }}>
        <div className={styles['register']}>
          <div className={styles['create-name']}>
            Tạo tài khoản mới
          </div>

          <form onSubmit={handleSubmit(onSubmit)}>
            <div className={styles['info']}>
              <div className={styles['email']}>
                <h6 className={styles['emailtxt']}>Email</h6>
                <input
                  className={styles['input-email']}
                  placeholder='Nhập email của bạn'
                  type='txt'
                  name='email'
                  type='email'
                  {...register('email', { required: true })}
                ></input>
              </div>
              {errors.email && (
                <span className={styles['erros']}>
                  This field is required
                </span>
              )}
              <div className={styles['pass']}>
                <h6 className={styles['passtxt']}> Mật khẩu</h6>
                <input
                  type={hide ? 'password' : 'text'}
                  className={styles['input-pass']}
                  placeholder='Nhập nhập mật khẩu mới của bạn'
                  name='pass'
                  type='password'
                  {...register(
                    'password',

                    { required: true, minLength: 8 },
                  )}
                ></input>
                <img
                  className={styles['hidden-pass']}
                  src={pass}
                  alt=''
                  onClick={() => {
                    setHide((hide) => !hide);
                  }}
                ></img>
              </div>
              {errors.password && (
                <span className={styles['erros']}>
                  pass không hợp lệ
                </span>
              )}
            </div>
            <div className={styles['check']}>
              <input
                type='checkbox'
                className={styles['checkb']}
              ></input>
              <div className={styles['name-check']}>
                Tôi đồng ý với <a>điều khoản sử dụng</a>
              </div>
            </div>
            <button type='submit' className={styles['btlogin']}>
              <div className={styles['regis']}>Đăng Ký</div>
            </button>
          </form>

          <img className={styles['or']} src={or}></img>
          <img className={styles['social']} src={social}></img>
          <div className={styles['login']}>
            Bạn đã có tài khoản?{' '}
            <a className={styles['link']}>Đăng nhập</a> tại đây
          </div>
        </div>
      </div>
    </div>
  );
};

export default Landing1st;
