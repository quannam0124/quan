import styles from './styles.module.scss';
import Contact from '../../components/Contact';
import FormRegister from './Components/Register.js';
import { useSelector } from 'react-redux';
import LoadingGlobal from '../../components/LoadingGlobal';

function App() {
  const loadingAuth = useSelector((state) => state.auth.loadingAuth);
  if (loadingAuth) {
    return <LoadingGlobal />;
  }
  return (
    <div className={styles['App']}>
      <div className={styles['BG-NavBar']}>
        <div className={styles['NavBar'] + ' ' + styles['container']}>
          <div className={styles['logo']}>
            <span>edunet</span>
          </div>
        </div>
      </div>
      <FormRegister />
      <Contact />
    </div>
  );
}

export default App;
