FROM node:14
WORKDIR /usr/src/app
COPY package.json /usr/src/app/
RUN npm install
COPY . /usr/src/app
RUN echo "Building ... "
RUN npm run build
EXPOSE 4200
CMD [ "npm", "run", "start", "--", "-p", "4200" ]
