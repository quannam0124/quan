import * as Types from '../constants/auth.js';
const initialState = {
  isLoged: false,
  user: null,
  loading: false,
  token: JSON.parse(localStorage.getItem('token')) || '',
  ready: false,
};
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.AUTH_CHANGE_STATE: {
      return {
        ...state,
        ...action.payload,
      };
    }
    case Types.LOGIN_SUCCESS: {
      localStorage.setItem(
        'token',
        JSON.stringify(action.payload.token),
      );
      return {
        ...state,
        ...action.payload,
        ready: true,
      };
    }
    case Types.COMPLETE_REGISTER_SUCCESS: {
      return {
        ...state,
        ...action.payload,
      };
    }
    case Types.CHANGE_ROLE_SUCCESS: {
      return {
        ...state,
        ...action.payload,
      };
    }
    case Types.LOGOUT: {
      localStorage.removeItem('token');
      return {
        ...initialState,
        token: null,
      };
    }
    case Types.NEW_PASSWORD_SUCCESS: {
      window.open('/login', '_self');
      return state;
    }
    default:
      return state;
  }
};
export default reducer;
