import * as Types from '../constants/resoures';
const initialState = {
  resources: [],
};
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.GET_RESOURCES_SUCCESS: {
      return {
        ...state,
        resources: action.payload,
      };
    }
    default:
      return state;
  }
};
export default reducer;
