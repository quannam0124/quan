import * as Types from '../constants/timetable';
const initialState = {
  timetable: [],
};
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.GET_STUDENT_LESSONS_SUCCESS: {
      return {
        ...state,
        timetable: action.payload,
      };
    }
    default:
      return state;
  }
};
export default reducer;
