import * as Types from '../constants/calendar';
const initialState = {
  schedules: [],
};
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.GET_CALENDAR_SUCCESS: {
      return {
        ...state,
        schedules: action.payload,
      };
    }
    default:
      return state;
  }
};
export default reducer;
