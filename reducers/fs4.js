import * as Types from '../constants/fs4';
const initialState = {
  knowledges: [],
  exercises: [],
};
const fs4Reducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.GET_KNOWLEDGE_SUCCESS: {
      return {
        ...state,
        knowledges: action.payload,
      };
    }
    case Types.GET_EXERCISE_SUCCESS: {
      return {
        ...state,
        exercises: action.payload,
      };
    }
    default:
      return state;
  }
};
export default fs4Reducer;
