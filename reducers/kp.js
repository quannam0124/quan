import * as Types from '../constants/kp';
const initialState = {
  subjects: [],
};
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.GET_SUBJECT_SUCCESS: {
      return {
        ...state,
        subjects: action.payload,
      };
    }
    default:
      return state;
  }
};
export default reducer;
