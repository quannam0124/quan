import { combineReducers } from 'redux';
import auth from './auth';
import kp from './kp';
import resource from './resource';
import timetable from './timetable';
import fs4 from './fs4';
import calendar from './calendar';
const appReducers = combineReducers({
  auth,
  kp,
  resource,
  timetable,
  fs4,
  calendar,
});

export default appReducers;
